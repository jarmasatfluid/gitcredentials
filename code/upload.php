<?php
session_start();
if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: login.php");

}


// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $target_dir = "./uploads/";
    $fileTmpPath = $_FILES['fileToUpload']['tmp_name'];
    $fileName = $_FILES['fileToUpload']['name'];
    $fileSize = $_FILES['fileToUpload']['size'];
    $fileType = $_FILES['fileToUpload']['type'];
    $fileNameCmps = explode(".", $fileName);
    $fileNamex = strtolower(current($fileNameCmps));
    $fileExtension = strtolower(end($fileNameCmps));
    $newFileName = md5($fileNamex) . '.' . $fileExtension;
    $dest_path = $target_dir . $newFileName;

    if( ( $fileType == "image/jpeg" || $fileType == "image/png" ) &&
    ( $fileSize < 100000 ) ) {
      if(move_uploaded_file($fileTmpPath, $dest_path))
    {
      $message ='File ' . $fileNamex . ' is successfully uploaded.';
    }
    else
    {
      $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
    }
    echo $message;}
    else{
      echo 'File not supported, only PNG images';
    }
}
?>
